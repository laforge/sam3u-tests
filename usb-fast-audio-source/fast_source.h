
#include <stdint.h>

#define AUDDLoopRecDriver_SAMPLERATE	640000
#define	AUDDLoopRecDriver_NUMCHANNELS	2
#define AUDDLoopRecDriver_BYTESPERSAMPLE 2

#define AUDDLoopRecDriver_SAMPLESPERFRAME (AUDDLoopRecDriver_SAMPLERATE / 5000 \
					   * AUDDLoopRecDriver_NUMCHANNELS)

#define AUDDLoopRecDriver_BYTESPERFRAME (AUDDLoopRecDriver_SAMPLESPERFRAME * \
					 AUDDLoopRecDriver_BYTESPERSAMPLE)

#include <usb/common/core/USBGenericRequest.h>

void fastsource_init(void);
void fastsource_start(void);
void fastsource_req_hdlr(const USBGenericRequest *request);
