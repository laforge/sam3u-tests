#include <stdlib.h>

#include <board.h>

#include <usb/common/core/USBGenericDescriptor.h>
#include <usb/common/core/USBDeviceDescriptor.h>
#include <usb/common/core/USBConfigurationDescriptor.h>
#include <usb/common/core/USBInterfaceDescriptor.h>
#include <usb/common/core/USBEndpointDescriptor.h>
//#include <usb/common/core/USBStringDescriptor.h>

#include <usb/device/core/USBDDriver.h>
#include <usb/device/core/USBDDriverDescriptors.h>

#define MIN(a, b)       ((a < b) ? a : b)

#define ISOCH

struct perf_conf_descs {
	USBConfigurationDescriptor configuration;
	USBInterfaceDescriptor interface;
	USBEndpointDescriptor bulkOut;
	USBEndpointDescriptor bulkIn;
	USBEndpointDescriptor bulkIn5;
} __attribute__((packed));

static const USBDeviceDescriptor dev_desc = {
	.bLength = sizeof(USBDeviceDescriptor),
	.bDescriptorType = USBGenericDescriptor_DEVICE,
	.bcdUSB = USBDeviceDescriptor_USB2_00,
	.bDeviceClass = 0xff,
	.bDeviceSubClass = 0xff,
	.bDeviceProtocol = 0xff,
	.bMaxPacketSize0 = CHIP_USB_ENDPOINTS_MAXPACKETSIZE(0),
	.idVendor = 0x16c0,
	.idProduct = 0x0763,
	.bcdDevice = 0x0100,
	.iManufacturer = 0,
	.iProduct = 0,
	.iSerialNumber = 0,
	.bNumConfigurations = 1,
};

static const USBDeviceQualifierDescriptor qual_desc = {
	.bLength = sizeof(USBDeviceQualifierDescriptor),
	.bDescriptorType = USBGenericDescriptor_DEVICEQUALIFIER,
	.bcdUSB = USBDeviceDescriptor_USB2_00,
	.bDeviceClass = 0xff,
	.bDeviceSubClass = 0,
	.bDeviceProtocol = 0xff,
	.bMaxPacketSize0 = CHIP_USB_ENDPOINTS_MAXPACKETSIZE(0),
	.bNumConfigurations = 1,
	.bReserved = 0,
};

static const struct perf_conf_descs conf_desc = {
	.configuration = {
		.bLength = sizeof(USBConfigurationDescriptor),
		.bDescriptorType = USBGenericDescriptor_CONFIGURATION,
		.wTotalLength = sizeof(struct perf_conf_descs),
		.bNumInterfaces = 1,
		.bConfigurationValue = 1,
		.iConfiguration = 0,
		.bmAttributes = BOARD_USB_BMATTRIBUTES,
		.bMaxPower = USBConfigurationDescriptor_POWER(100),
	},
	.interface = {
		.bLength = sizeof(USBInterfaceDescriptor),
		.bDescriptorType = USBGenericDescriptor_INTERFACE,
		.bInterfaceNumber = 0,
		.bAlternateSetting = 0,
		.bNumEndpoints = 3,
		.bInterfaceClass = 0xff,
		.bInterfaceSubClass = 0,
		.bInterfaceProtocol = 0xff,
		.iInterface = 0,
	},
	.bulkOut = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_OUT, 1),
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(1), USBEndpointDescriptor_MAXBULKSIZE_FS),
		.bInterval = 0,
	},
	.bulkIn = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_IN, 2),
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(2), USBEndpointDescriptor_MAXBULKSIZE_FS),
		.bInterval = 0,
	},
	.bulkIn5 = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_IN, 5),
#ifdef ISOCH
		.bmAttributes = USBEndpointDescriptor_ISOCHRONOUS,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(5), USBEndpointDescriptor_MAXISOCHRONOUSSIZE_FS),
		.bInterval = 0,
#else
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(5), USBEndpointDescriptor_MAXBULKSIZE_FS),
		.bInterval = 0x04,
#endif
	},
};
static const struct perf_conf_descs conf_desc_hs = {
	.configuration = {
		.bLength = sizeof(USBConfigurationDescriptor),
		.bDescriptorType = USBGenericDescriptor_CONFIGURATION,
		.wTotalLength = sizeof(struct perf_conf_descs),
		.bNumInterfaces = 1,
		.bConfigurationValue = 1,
		.iConfiguration = 0,
		.bmAttributes = BOARD_USB_BMATTRIBUTES,
		.bMaxPower = USBConfigurationDescriptor_POWER(100),
	},
	.interface = {
		.bLength = sizeof(USBInterfaceDescriptor),
		.bDescriptorType = USBGenericDescriptor_INTERFACE,
		.bInterfaceNumber = 0,
		.bAlternateSetting = 0,
		.bNumEndpoints = 3,
		.bInterfaceClass = 0xff,
		.bInterfaceSubClass = 0,
		.bInterfaceProtocol = 0xff,
		.iInterface = 0,
	},
	.bulkOut = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_OUT, 1),
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(1), USBEndpointDescriptor_MAXBULKSIZE_HS),
		.bInterval = 0,
	},
	.bulkIn = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_IN, 2),
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(2), USBEndpointDescriptor_MAXBULKSIZE_HS),
		.bInterval = 0,
	},
	.bulkIn5 = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_IN, 5),

#ifdef ISOCH
		.bmAttributes = USBEndpointDescriptor_ISOCHRONOUS,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(5), USBEndpointDescriptor_MAXISOCHRONOUSSIZE_HS),
		.bInterval = 0,
#else
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(5), USBEndpointDescriptor_MAXBULKSIZE_HS),
		.bInterval = 0,
#endif
	},
};

static const struct perf_conf_descs conf_desc_other_fs = {
	.configuration = {
		.bLength = sizeof(USBConfigurationDescriptor),
		.bDescriptorType = USBGenericDescriptor_OTHERSPEEDCONFIGURATION,
		.wTotalLength = sizeof(struct perf_conf_descs),
		.bNumInterfaces = 1,
		.bConfigurationValue = 1,
		.iConfiguration = 0,
		.bmAttributes = BOARD_USB_BMATTRIBUTES,
		.bMaxPower = USBConfigurationDescriptor_POWER(100),
	},
	.interface = {
		.bLength = sizeof(USBInterfaceDescriptor),
		.bDescriptorType = USBGenericDescriptor_INTERFACE,
		.bInterfaceNumber = 0,
		.bAlternateSetting = 0,
		.bNumEndpoints = 3,
		.bInterfaceClass = 0xff,
		.bInterfaceSubClass = 0,
		.bInterfaceProtocol = 0xff,
		.iInterface = 0,
	},
	.bulkOut = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_OUT, 1),
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(1), USBEndpointDescriptor_MAXBULKSIZE_FS),
		.bInterval = 0,
	},
	.bulkIn = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_IN, 2),
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(2), USBEndpointDescriptor_MAXBULKSIZE_FS),
		.bInterval = 0,
	},
	.bulkIn5 = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_IN, 5),
#ifdef ISOCH
		.bmAttributes = USBEndpointDescriptor_ISOCHRONOUS,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(5), USBEndpointDescriptor_MAXISOCHRONOUSSIZE_FS),
		.bInterval = 0x04,
#else
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(5), USBEndpointDescriptor_MAXBULKSIZE_FS),
		.bInterval = 0,
#endif
	},
};
static const struct perf_conf_descs conf_desc_other_hs = {
	.configuration = {
		.bLength = sizeof(USBConfigurationDescriptor),
		.bDescriptorType = USBGenericDescriptor_OTHERSPEEDCONFIGURATION,
		.wTotalLength = sizeof(struct perf_conf_descs),
		.bNumInterfaces = 1,
		.bConfigurationValue = 1,
		.iConfiguration = 0,
		.bmAttributes = BOARD_USB_BMATTRIBUTES,
		.bMaxPower = USBConfigurationDescriptor_POWER(100),
	},
	.interface = {
		.bLength = sizeof(USBInterfaceDescriptor),
		.bDescriptorType = USBGenericDescriptor_INTERFACE,
		.bInterfaceNumber = 0,
		.bAlternateSetting = 0,
		.bNumEndpoints = 3,
		.bInterfaceClass = 0xff,
		.bInterfaceSubClass = 0,
		.bInterfaceProtocol = 0xff,
		.iInterface = 0,
	},
	.bulkOut = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_OUT, 1),
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(1), USBEndpointDescriptor_MAXBULKSIZE_HS),
		.bInterval = 0,
	},
	.bulkIn = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_IN, 2),
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(2), USBEndpointDescriptor_MAXBULKSIZE_HS),
		.bInterval = 0,
	},
	.bulkIn5 = {
		.bLength = sizeof(USBEndpointDescriptor),
		.bDescriptorType = USBGenericDescriptor_ENDPOINT,
		.bEndpointAddress = USBEndpointDescriptor_ADDRESS(USBEndpointDescriptor_IN, 5),
#ifdef ISOCH
		.bmAttributes = USBEndpointDescriptor_ISOCHRONOUS,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(5), USBEndpointDescriptor_MAXISOCHRONOUSSIZE_HS),
		.bInterval = 0x04,
#else
		.bmAttributes = USBEndpointDescriptor_BULK,
		.wMaxPacketSize = MIN(CHIP_USB_ENDPOINTS_MAXPACKETSIZE(5), USBEndpointDescriptor_MAXBULKSIZE_HS),
		.bInterval = 0,
#endif
	},
};

const USBDDriverDescriptors usb_perf_driver_desc = {
	.pFsDevice = &dev_desc,
	.pFsConfiguration = &conf_desc,
	.pFsQualifier = &qual_desc,
	.pFsOtherSpeed = &conf_desc_other_fs,
	.pHsDevice = &dev_desc,
	.pHsConfiguration = &conf_desc_hs,
	.pHsQualifier = &qual_desc,
	.pHsOtherSpeed = &conf_desc_other_hs,
	.pStrings = NULL,
	.numStrings = 0,
};

#if 0
static USBDDriver perftest_usbdriver = {
	.pDescriptors 	= &driver_desc,
	//.pInterfaces 	=
};
#endif
