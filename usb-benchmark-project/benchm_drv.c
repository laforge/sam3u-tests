#include <stdlib.h>
#include <stdint.h>
#include <board.h>
#include <utility/trace.h>

#include <usb/common/core/USBGenericRequest.h>
#include <usb/device/core/USBD.h>
#include <usb/device/core/USBDDriver.h>
#include <usb/device/core/USBDDriverDescriptors.h>
#include <usb/device/core/USBDCallbacks.h>


extern const USBDDriverDescriptors usb_perf_driver_desc;

static unsigned char driver_interfaces[3];
static USBDDriver benchm_driver;


/* call-back for control EP requests from the host */
void USBDCallbacks_RequestReceived(const USBGenericRequest *req)
{
	/* forward all (other) requests to core */
	USBDDriver_RequestHandler(&benchm_driver, req);
}

void benchmark_drv_init(void)
{
	USBDDriver_Initialize(&benchm_driver, &usb_perf_driver_desc,
				driver_interfaces);
	USBD_Init();
}

static uint8_t test_data[8192];

static void wr_compl_cb(void *arg, unsigned char status, unsigned int transferred, unsigned int remain)
{
	uint8_t epnr = arg;

	/* re-start */
	if (status == 0 && remain == 0) {
		//printf(".");
		benchmark_start(epnr);
	} else
		printf("Err: EP%u wr_compl, status 0x%02u, xfr %u, remain %u\r\n",
			epnr, status, transferred, remain);
}

void benchmark_start(uint8_t epnr)
{
	int i;

	for (i = 0; i < sizeof(test_data); i++)
		test_data[i] = i & 0xff;

	USBD_Write(epnr, &test_data, sizeof(test_data), wr_compl_cb, epnr);
}
