/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ----------------------------------------------------------------------------
 * Copyright (c) 2009, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */
/** \cond usb_audio_looprec
 *  \page usb_audio_looprec USB Audio Loopback-Recorder Example
 *
 *  \section Purpose
 *
 *  The USB Audio Loopback-Recorder Example will help you to get
 *  familiar with the USB Device Port(UDP) and DACC on AT91SAM microcontrollers.
 *  Also it can help you to be familiar with the USB Framework that is used for
 *  rapid development of USB-compliant class drivers such as USB Audio Device
 *  class.
 *
 *  \section Requirements
 *
 *  This package can be used with SAM9XX5 evaluation kits that have both
 *  UDP.
 *
 *  \section Description
 *
 *  The demo simulates an USB Desktop Speaker with Microphone which actually
 *  does not "speak out" but loop back the sound as microphone input.
 *
 *  When an EK running this program connected to a host (PC for example), with
 *  USB cable, the EK appears as a desktop speaker for the host. Then the host
 *  can play sound through host software. The audio stream from the host is
 *  then sent to the EK. At the same time, the audio stream received is also
 *  sent back to host from EK for recording.
 *
 *  \section Usage
 *
 *  -# Build the program and download it inside the evaluation board. Please
 *     refer to the
 *     <a href="http://www.atmel.com/dyn/resources/prod_documents/doc6421.pdf">
 *     SAM-BA User Guide</a>, the
 *     <a href="http://www.atmel.com/dyn/resources/prod_documents/doc6310.pdf">
 *     GNU-Based Software Development</a> application note or to the
 *     <a href="ftp://ftp.iar.se/WWWfiles/arm/Guides/EWARM_UserGuide.ENU.pdf">
 *     IAR EWARM User Guide</a>, depending on your chosen solution.
 *  -# On the computer, open and configure a terminal application
 *     (e.g. HyperTerminal on Microsoft Windows) with these settings:
 *    - 115200 bauds
 *    - 8 bits of data
 *    - No parity
 *    - 1 stop bit
 *    - No flow control
 *  -# Start the application.
 *  -# In the terminal window, the following text should appear:
 *  \code
 *  -- USB Device Audio LoopREC Example xxx --
 *  -- AT91xxxxxx-xx
 *  -- Compiled: xxx xx xxxx xx:xx:xx --
 *  \endcode
 *  -# When connecting USB cable to windows, the LED blinks, and the host
 *     reports a new USB device attachment (if it's the first time you connect
 *     an audio speaker demo board to your host). You can find new
 *     "USB Composite Device" and "USB Audio Device" appear in the hardware
 *     device list.
 *  -# You can play sound in host side through the USB Audio Device.
 *     When playing sound, you can also record through the USB Audio Device on
 *     the host.
 *
 *  \section References
 *  - usb_audio_looprec/main.c
 *  - ssc: SSC interface driver
 *  - usb: USB Framework, Audio Device Class driver and UDP interface driver
 *      - \ref usbd_framework
 *         - \ref usbd_api
 *      - \ref usbd_audio_rec_drv
 *
 */

/**
 *  \file
 *
 *  This file contains all the specific code for the
 *  usb_audio_looprec example.
 */

/*----------------------------------------------------------------------------
 *         Headers
 *----------------------------------------------------------------------------*/

#include <board.h>

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdbool.h>

#include <irq/irq.h>
#include <pio/pio.h>
#include <pio/pio_it.h>
#include <utility/trace.h>
#include <utility/led.h>

#include <usb/device/core/USBD.h>
#include <usb/device/audio-looprec/AUDDLoopRecDriver.h>
#include <usb/device/audio-looprec/AUDDLoopRecChannel.h>
/*----------------------------------------------------------------------------
 *         Definitions
 *----------------------------------------------------------------------------*/

/**  Number of available audio buffers. */
#define BUFFER_NUMBER       8
/**  Size of one buffer in bytes. */
#define BUFFER_SIZE         (AUDDLoopRecDriver_BYTESPERFRAME*2)

/// Use for power management
#define STATE_IDLE    0
/// The USB device is in suspend state
#define STATE_SUSPEND 4
/// The USB device is in resume state
#define STATE_RESUME  5

//------------------------------------------------------------------------------
//         Internal variables
//------------------------------------------------------------------------------
/// State of USB, for suspend and resume
unsigned char USBState = STATE_IDLE;

/**  Data buffers for receiving audio frames from the USB host. */
static uint8_t buffers[BUFFER_NUMBER][BUFFER_SIZE];
/**  Number of samples stored in each data buffer. */
static uint32_t bufferSizes[BUFFER_NUMBER];
/**  Next buffer in which USB data can be stored. */
static uint32_t inBufferIndex = 0;
/**  Number of buffers that can be sent to the DAC. */
static volatile uint32_t numBuffersToSend = 0;

/**  Current state of the playback stream interface. */
static volatile uint8_t isPlyActive = 0;
/**  Current state of the record stream interface. */
static volatile uint8_t isRecActive = 0;

/*----------------------------------------------------------------------------
 *         VBus monitoring (optional)
 *----------------------------------------------------------------------------*/

/**  VBus pin instance. */
static const Pin pinVbus = PIN_USB_VBUS;

/**
 *  Handles interrupts coming from PIO controllers.
 */
static void ISR_Vbus(const Pin *pPin)
{
    /* Check current level on VBus */
    if (PIO_Get(&pinVbus))
    {
        TRACE_INFO("VBUS conn\n\r");
        USBD_Connect();
    }
    else
    {
        TRACE_INFO("VBUS discon\n\r");
        USBD_Disconnect();
    }
}

/**
 *  Configures the VBus pin to trigger an interrupt when the level on that pin
 *  changes.
 */
static void VBus_Configure( void )
{
    /* Configure PIO */
    PIO_Configure(&pinVbus, 1);
    PIO_ConfigureIt(&pinVbus, ISR_Vbus);
    PIO_EnableIt(&pinVbus);

    /* Check current level on VBus */
    if (PIO_Get(&pinVbus))
    {
        /* if VBUS present, force the connect */
        USBD_Connect();
    }
    else
    {
        TRACE_INFO("discon\n\r");
        USBD_Disconnect();
    }
}

/*----------------------------------------------------------------------------
 *         USB Power Control
 *----------------------------------------------------------------------------*/

#ifdef PIN_USB_POWER_ENA
/** Power Enable A (MicroAB Socket) pin instance. */
static const Pin pinPOnA = PIN_USB_POWER_ENA;
#endif
#ifdef PIN_USB_POWER_ENB
/** Power Enable B (A Socket) pin instance. */
static const Pin pinPOnB = PIN_USB_POWER_ENB;
#endif
#ifdef PIN_USB_POWER_ENC
/** Power Enable C (A Socket) pin instance. */
static const Pin pinPOnC = PIN_USB_POWER_ENC;
#endif
/**
 * Configures the Power Enable pin to disable self power.
 */
static void USBPower_Configure( void )
{
  #ifdef PIN_USB_POWER_ENA
    PIO_Configure(&pinPOnA, 1);
  #endif
  #ifdef PIN_USB_POWER_ENB
    PIO_Configure(&pinPOnB, 1);
  #endif
  #ifdef PIN_USB_POWER_ENC
    PIO_Configure(&pinPOnC, 1);
  #endif
}

/*----------------------------------------------------------------------------
 *         Internal functions
 *----------------------------------------------------------------------------*/

/**
 *  Invoked when a frame has been received.
 */
static void FrameReceived(uint32_t unused,
                          uint8_t status,
                          uint32_t transferred,
                          uint32_t remaining)
{
    if (status == USBD_STATUS_SUCCESS)
    {
        /* Loopback! add this buffer to write list */
        if (!isRecActive)  {}
        else
        {
            AUDDLoopRecDriver_Write(buffers[inBufferIndex],
                                         AUDDLoopRecDriver_BYTESPERFRAME,
					 NULL, 0);
        }

        /* Update input status data */
        bufferSizes[inBufferIndex] = transferred
                                        / AUDDLoopRecDriver_BYTESPERSAMPLE;
        inBufferIndex = (inBufferIndex + 1) % BUFFER_NUMBER;
        numBuffersToSend++;

    }
    else if (status == USBD_STATUS_ABORTED)
    {
        /* Error , ABORT, add NULL buffer */
        bufferSizes[inBufferIndex] = 0;
        inBufferIndex = (inBufferIndex + 1) % BUFFER_NUMBER;
        numBuffersToSend++;
    }
    else
    {
        /* Packet is discarded */
    }

    /* Receive next packet */
    AUDDLoopRecDriver_Read(buffers[inBufferIndex],
                           AUDDLoopRecDriver_BYTESPERFRAME,
                           (TransferCallback) FrameReceived,
                           0); // No optional argument
}

/*----------------------------------------------------------------------------
 *         Callbacks re-implementation
 *----------------------------------------------------------------------------*/

//------------------------------------------------------------------------------
/// Invoked when the USB device leaves the Suspended state. By default,
/// configures the LEDs.
//------------------------------------------------------------------------------
void USBDCallbacks_Resumed(void)
{
    // Initialize LEDs
    LED_Configure(USBD_LEDPOWER);
    LED_Set(USBD_LEDPOWER);
    LED_Configure(USBD_LEDUSB);
    LED_Clear(USBD_LEDUSB);
    USBState = STATE_RESUME;
}

//------------------------------------------------------------------------------
/// Invoked when the USB device gets suspended. By default, turns off all LEDs.
//------------------------------------------------------------------------------
void USBDCallbacks_Suspended(void)
{
    // Turn off LEDs
    LED_Clear(USBD_LEDPOWER);
    LED_Clear(USBD_LEDUSB);
    USBState = STATE_SUSPEND;
}



/**
 *  Invoked when an audio channel get muted or unmuted. Mutes/unmutes the
 *  channel at the DAC level.
 *  \param mic      Microphone/Speaker stream changed.
 *  \param channel  Channel number that changed.
 *  \param muted    Indicates the new mute status of the channel.
 */
void AUDDLoopRecChannel_MuteChanged(AUDDLoopRecChannel *channel,
                                        uint8_t muted)
{
    /* Speaker Master channel */
    if (channel == AUDDLoopRecDriver_MASTERCHANNEL)
    {
        if (muted)
        {
            TRACE_WARNING("MuteMaster ");
        }
        else
        {
            TRACE_INFO("UnmuteMaster ");
        }
    }
}

#if 0
/**
 *  Invoked when an audio streaming interface setting changed.
 *  Audio stream is automatically reseted.
 *  Actually control streaming rate.
 *  \param mic         Microphone/Speaker stream changed.
 *  \param newSetting  New stream (interface) setting.
 */
void AUDDLoopRecDriver_StreamSettingChanged(uint8_t mic,
                                                 uint8_t newSetting)
{
    /* Speaker stream */
    if (!mic)
    {
        isPlyActive = (newSetting > 0);
    }
    else
    {
        isRecActive = (newSetting > 0);
    }
}
#endif

/*----------------------------------------------------------------------------
 *         Exported functions
 *----------------------------------------------------------------------------*/

extern void USBD_IrqHandler(void);
/**
 *  \brief usb_audio_looprec Application entry point.
 *
 *  Starts the driver and waits for an audio input stream to forward to the DAC.
 */
int main(void)
{
    volatile uint8_t usbConn = 0;
    volatile uint8_t plyOn = 0, recOn = 0;

    printf("-- USB Device Audio LoopREC Example %s --\n\r", SOFTPACK_VERSION);
    printf("-- %s\n\r", BOARD_NAME);
    printf("-- Compiled: %s %s --\n\r", __DATE__, __TIME__);

    /* If they are present, configure Vbus & Wake-up pins */
    PIO_InitializeInterrupts(0);

    /* Initialize all USB power (off) */
    USBPower_Configure();

    /* Audio STREAM LED */
    LED_Configure(USBD_LEDOTHER);

    /* Configure USB & DACC interrupt priority */
    //IRQ_ConfigureIT( ID_UDPHS, 6, USBD_IrqHandler );

    /* USB audio driver initialization */
    AUDDLoopRecDriver_Initialize();

    /* connect if needed */
    VBus_Configure();

    /* Infinite loop */
    while (1)
    {
        if (USBD_GetState() < USBD_STATE_CONFIGURED)
        {
            usbConn = 0;
            continue;
        }
        if (plyOn)
        {
            if (isPlyActive == 0)
            {
                printf("plyE ");
                plyOn = 0;
            }
        }
        else if (isPlyActive)
        {
            /* Try to Start Reading the incoming audio stream */
            AUDDLoopRecDriver_Read(buffers[inBufferIndex],
                                   AUDDLoopRecDriver_BYTESPERFRAME,
                                   (TransferCallback) FrameReceived,
                                   0); // No optional argument

            printf("plyS ");
            plyOn = 1;
        }
        if (recOn)
        {
            if (isRecActive == 0)
            {
                printf("recE ");
                recOn = 0;
            }
        }
        else if (isRecActive)
        {
            printf("recS ");
            recOn = 1;
        }
    }
}
/** \endcond */
